import csv
from collections import defaultdict

columns = defaultdict(list) # each value in each column is appended to a list

with open('ysp.csv', 'rb') as f:
    reader = csv.DictReader(f) # read rows into a dictionary format
    for row in reader: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value 
            columns[k].append(v) # append the value into the appropriate list
                                 # based on column name k

gender = columns['Gender']
male = 0
female = 0
for each in gender:
	if (each == "Female"):
		female += 1
	else: male += 1
print "Male: ", male
print "Female: ", female

birthday = columns['Birth Date']

year00 = 0
year01 = 0
year99 = 0
year98 = 0
year97 = 0

for each in birthday:
	year = each[-2:]
	if (year == '01'): year01 += 1
	elif (year == '00'): year00 += 1
	elif (year == '99'): year99 += 1
	elif (year == '98'): year98 += 1
	elif (year == '97'): year97 += 1


print
print "1997: ", year97
print "1998: ", year98
print "1999: ", year99
print "2000: ", year00
print "2001: ", year01

schools = columns['School']
hobbySchool = 0
englishSchoolofMongolia = 0
newEra = 0
santSchool = 0
empathySchool = 0
orchlonSchool = 0
russianSchool = 0
newBeginning = 0
for each in schools:
	if ("Hobby" in each):
		hobbySchool+= 1
	elif ("English" in each):
		englishSchoolofMongolia += 1
	elif ("New Era" in each) or ("New era" in each):
		newEra += 1
	elif ("Sant" in each) or ("sant" in each):
		santSchool += 1
	elif ("Empathy" in each) or ("empathy" in each):
		empathySchool += 1
	elif ("Orchlon" in each) or ("orchlon" in each):
		orchlonSchool += 1
	elif ("oros" in each) or ("Oros" in each) or ("Russian" in each):
		russianSchool += 1
	elif ("Beginning" in each) or ("beginning" in each):
		newBeginning += 1

print
print "Hobby School:", hobbySchool
print "English School of Mongolia:", englishSchoolofMongolia
print "New Era International School:", newEra
print "Sant School:", santSchool
print "Empathy School:", empathySchool
print "Orchlon School:", orchlonSchool
print "Russian School:", russianSchool
print "New Beginning School:", newBeginning
print hobbySchool+englishSchoolofMongolia+newEra+newBeginning+santSchool+empathySchool+orchlonSchool+russianSchool

grades = columns["Grade"]
tenthgrade = 0
eleventhgrade = 0
twelfthgrade = 0
firstyear = 0
for each in grades:
	if ("10" in each): tenthgrade += 1
	elif ("11" in each): eleventhgrade += 1
	elif ("12" in each): twelfthgrade += 1
	elif ("Bachelor" in each): firstyear += 1

print
print "10th grade:", tenthgrade
print "11th grade:", eleventhgrade
print "12th grade:", twelfthgrade
print "Bachelor's:", firstyear

