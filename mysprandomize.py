import csv
from collections import defaultdict

columns = defaultdict(list) # each value in each column is appended to a list

with open('ysp.csv', 'rb') as f:
    reader = csv.DictReader(f) # read rows into a dictionary format
    for row in reader: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value 
            columns[k].append(v) 


first = columns['First']
#print first

import random
numbers = list(xrange(0, 30))
random.shuffle(numbers)
#print numbers

emptylist = []
for each in numbers:
	emptylist.append(first[each])

print emptylist


