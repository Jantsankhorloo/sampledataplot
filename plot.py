import csv
from collections import defaultdict
import matplotlib.pyplot as plt
import datetime
import numpy as np

columns = defaultdict(list) # each value in each column is appended to a list
#FOR JUNE 9
with open('data.csv', 'rb') as f:
    reader = csv.DictReader(f) # read rows into a dictionary format
    for row in reader: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value 
            columns[k].append(v) 

name = columns["Name"]
load = columns["Load"]
loadData = []

listofIndex = [i for i, x in enumerate(name) if x == "N.Y.C."]

for each in listofIndex:
	loadData.append(load[each])

x = np.array([datetime.datetime(2016, 6, 9, i, j) for i in range(24) for j in xrange(0,60,5)])

print max(load)

#FOR 2016 JUNE 3
columns1 = defaultdict(list)
with open('data1.csv', 'rb') as file:
    reader1 = csv.DictReader(file) # read rows into a dictionary format
    for row in reader1: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value 
            columns1[k].append(v) 

name1 = columns1["Name"]
load1 = columns1["Load"]
loadData1 = []

listofIndex1 = [i for i, y in enumerate(name1) if y == "N.Y.C."]

for each in listofIndex1:
	loadData1.append(load1[each])

#JUNE 4
columns2 = defaultdict(list)
with open('data2.csv', 'rb') as f2:
    reader2 = csv.DictReader(f2) # read rows into a dictionary format
    for row in reader2: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value 
            columns2[k].append(v) 

name2 = columns2["Name"]
load2 = columns2["Load"]
loadData2 = []
listofIndex2 = [i for i, z in enumerate(name2) if z == "N.Y.C."]

for each in listofIndex2:
    loadData2.append(load2[each])

#JUNE 5
columns3 = defaultdict(list)
with open('data3.csv', 'rb') as f3:
    reader3 = csv.DictReader(f3) # read rows into a dictionary format
    for row in reader3: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value 
            columns3[k].append(v) 

name3 = columns3["Name"]
load3 = columns3["Load"]
loadData3 = []
listofIndex3 = [i for i, w in enumerate(name3) if w == "N.Y.C."]
listofIndex3 = listofIndex3[:-2]

for each in listofIndex3:
    loadData3.append(load3[each])

#JUNE 6
columns4 = defaultdict(list)
with open('data4.csv', 'rb') as f4:
    reader4 = csv.DictReader(f4) # read rows into a dictionary format
    for row in reader4: # read a row as {column1: value1, column2: value2,...}
        for (k,v) in row.items(): # go over each column name and value 
            columns4[k].append(v) 

name4 = columns4["Name"]
load4 = columns4["Load"]
loadData4 = []
listofIndex4 = [i for i, t in enumerate(name4) if t == "N.Y.C."]
listofIndex4 = listofIndex4[:-19]
for each in listofIndex4:
    loadData4.append(load4[each])

plt.plot(x, loadData, color = "blue", label = "June 3")
plt.plot(x, loadData2, color = "green", label = "June 4")
plt.plot(x, loadData3, color = "yellow", label = "June 5")
plt.plot(x, loadData4, color = "purple", label = "June 6")
plt.plot(x, loadData1, color = "red", label = "June 7")
plt.title("New York City Real Time Electricity Load Plot")
plt.xlabel("June of 2016")
plt.ylabel("Zone Load in MW")
plt.legend()
plt.show()



