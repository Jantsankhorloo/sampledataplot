import datetime
import matplotlib.pyplot as plt

one = [22,23,18,13,8,9,	8,8,8,11]
two = [34,34,34,34,34,36,34,34,34,34]
three = [43,42,35,26,26,24,24,22,28,35]
four = [133,139,115,74,75,69,74,69,70,107]
five = [40,40,37,27,27,27,40,30,28,32]
six = [858,568,552,536,582,416,372,472,554,600]
seven = [566,542,517,428,512,395,386,472,460,452]
eight = [215,110,195,145,130,105,140,135,135,165]
nine = [75,98,60,70,85,130,160,110,110,110]
ten = [99,98,96,80,92,72,95,100,103,111]


months = [1,2,3,4,5,6,7,8,9,10]

plt.plot(months, one)
plt.plot(months, two)
plt.plot(months, three)
plt.plot(months, four)
plt.plot(months, five)
plt.plot(months, six)
plt.plot(months, seven)
plt.plot(months, eight)
plt.plot(months, nine)
plt.plot(months, ten)
plt.show()
